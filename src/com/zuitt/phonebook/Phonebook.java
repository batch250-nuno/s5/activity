package com.zuitt.phonebook;

import java.util.ArrayList;
import com.zuitt.phonebook.Contact;

public class Phonebook {
    ArrayList<Contact> contacts = new ArrayList<Contact>();

    private Contact contact;

    public Phonebook() {

    };
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContact() {
        for (Contact contact: contacts) {
            System.out.println(contact.getName());
        }
//        System.out.println(contacts.size());
        return contacts;
    }
    public Contact getContact(String name){
        for (Contact contact: contacts) {
            if(contact.getName() == name){
                return contact;
            }
        }
        return null;
    }

    public Phonebook setContact(Contact contact) {
        contacts.add(contact);
        return this;
    }

    public void printInfo(){
        if(contacts.size() != 0){
            for (Contact contact: contacts) {
                System.out.println("-------------------------------");
                System.out.println(contact.getName());
                System.out.println("-------------------------------");
                System.out.println(contact.getName() +" has the following registered number:");
                if(contact.getContactNumber() != null){
                    System.out.println(contact.getContactNumber());
                }else{
                    System.out.println("There is no registered contacts");
                }
                System.out.println(contact.getAddress() +" has the following registered address:");
                if(contact.getAddress() != null){
                    System.out.println(contact.getAddress());
                }else{
                    System.out.println("There is no registered address");
                }
            }
        } else {
            System.out.println("Phonebook is Empty");
        }
    }

    public void printInfo(String name){
        Contact retrievedContact = getContact(name);
        if(retrievedContact != null){
            System.out.println(name);
            System.out.println("-------------------------------");
            System.out.println(name +" has the following registered number:");
            if(retrievedContact.getContactNumber() != null){
                System.out.println(retrievedContact.getContactNumber());
            }else{
                System.out.println("No registered contacts");
            }
            System.out.println(name +" has the following registered address:");
            if(retrievedContact.getAddress() != null){
                System.out.println(retrievedContact.getAddress());
            }else{
                System.out.println("No registered address");
            }
        }else{
            System.out.println("No Contact info :" + name);
        }
    }



}
