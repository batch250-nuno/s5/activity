import com.zuitt.phonebook.Contact;
import com.zuitt.phonebook.Phonebook;

public class Main {
    public static void main(String[] args) {
        Contact contact1 = new Contact();
        contact1.setName("test");
        contact1.setContactNumber("1234567");
        contact1.setAddress("testAddress");

        Contact contact2 = new Contact();
        contact2.setName("test2");
        contact2.setContactNumber("01234567");
        contact2.setAddress("test2Address");

        Phonebook phonebook = new Phonebook();
        phonebook.setContact(contact1);
        phonebook.setContact(contact2);

//        phonebook.getContact();
        phonebook.printInfo("asd");
//        phonebook.printInfo("test2");

    }
}
